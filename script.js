console.log(`hello`)

/* JSON
Javascript Object Notation
	-data format used by app to store and transport data to one another
	-not limited to js based apps, can be used in other prog languages
	-saved in file ext .json
*/

// console.log({
// 	"city": "Quezon City",
// 	"province": "Metro Manila",
// 	"country": "Philippines"
// })


console.log("This is a JavaSript Array:");

let batchArr = [
	{ name: "Angelito",
	batch: "B163"},
	{ name: "Ian",
	batch: "B163"}
]

console.log(batchArr)

batchArr.forEach(data=>console.log(data))

/*stringify-to convert js objec and arrays to json*/

const convertedToStr = JSON.stringify(batchArr);
console.log(convertedToStr)

// convertedToStr.forEach(data => console.log(data))
	// not possible to use JS methods if the state is still in json/string

console.log(JSON.parse(convertedToStr))

const convertedToJS = JSON.parse(convertedToStr)
convertedToJS.forEach(data =>console.log(data))